"""empty message

Revision ID: fa0e77c843c3
Revises: c36726105ff3
Create Date: 2021-07-26 17:48:48.983620

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fa0e77c843c3'
down_revision = 'c36726105ff3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('turno_trabalho', sa.Column('dia_da_semana', sa.Integer(), nullable=True))
    op.add_column('turno_trabalho', sa.Column('start_at', sa.String(), nullable=True))
    op.add_column('turno_trabalho', sa.Column('stop_at', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('turno_trabalho', 'stop_at')
    op.drop_column('turno_trabalho', 'start_at')
    op.drop_column('turno_trabalho', 'dia_da_semana')
    # ### end Alembic commands ###
