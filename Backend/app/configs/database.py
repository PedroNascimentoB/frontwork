from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db: SQLAlchemy = SQLAlchemy()


def init_app(app: Flask) -> None:
    db.init_app(app)
    app.db = db

    from app.models.turnos_model import TurnoModel
    from app.models.calendario_model import CalendarioModel
    from app.models.turno_trabalho_model import TurnoTrabalhoModel
