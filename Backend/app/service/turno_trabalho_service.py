def set_days(day: int):
    if int(day) == 1:
        return (1, "Segunda-Feira")
    if int(day) == 2:
        return (2, "Terça-Feira")
    if int(day) == 3:
        return (3, "Quarta-Feira")
    if int(day) == 4:
        return (4, "Quinta-Feira")
    if int(day) == 5:
        return (5, "Sexta-Feira")

    return False


def hora_minuto(str: str):

    hora = int(str.split(':')[0])
    minuto = int(str.split(':')[1])

    return (hora, minuto)


def comparar(inicioH, inicioM, fimH, fimM):
    if inicioH > fimH:
        return False
    if inicioH == fimH and inicioM > fimM:
        return False

    return True
