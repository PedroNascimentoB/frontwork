from flask import Blueprint, jsonify, request

from app.models.calendario_model import CalendarioModel
from http import HTTPStatus
from flask_cors import CORS, logging

bp_calendario = Blueprint('bp_calendario', __name__)

CORS(bp_calendario)


@bp_calendario.route("/calendar/list", methods=["GET"])
def get_all():
    calendario: CalendarioModel = CalendarioModel.query.all()
    if not calendario:
        return "Nenhum calendario cadastrado", HTTPStatus.NO_CONTENT
    return jsonify(calendario)
