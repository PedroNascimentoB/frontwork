from flask import Flask


def init_app(app: Flask):
    from .turno_view import bp_turno
    from .calendar_view import bp_calendario
    from .turno_trabalho_view import bp_turno_trabalho

    app.register_blueprint(bp_turno)
    app.register_blueprint(bp_calendario)
    app.register_blueprint(bp_turno_trabalho)
