from flask import Blueprint, jsonify, request

from app.models.turnos_model import TurnoModel
from http import HTTPStatus
from flask_cors import CORS, logging

bp_turno = Blueprint('bp_turno', __name__)

CORS(bp_turno)


@bp_turno.route("/shifts/list", methods=["GET"])
def get_all():
    turno: TurnoModel = TurnoModel.query.all()
    if not turno:
        return {"message": "Nenhum turno cadastrado"}, HTTPStatus.BAD_REQUEST
    return jsonify(turno)


logging.getLogger('flask_cors').level = logging.DEBUG
