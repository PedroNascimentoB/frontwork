from flask import Blueprint, jsonify, request, current_app

from app.models.turno_trabalho_model import TurnoTrabalhoModel
from app.models.turnos_model import TurnoModel
from app.models.calendario_model import CalendarioModel
from app.service.turno_trabalho_service import set_days, hora_minuto, comparar
from flask_cors import CORS
from http import HTTPStatus

bp_turno_trabalho = Blueprint('bp_turno_trabalho', __name__)

CORS(bp_turno_trabalho)

@bp_turno_trabalho.route("/workShift/list", methods=["GET"])
def get_all():
    turno_trabalho: TurnoTrabalhoModel = TurnoTrabalhoModel.query.all()
    if not turno_trabalho:
        return {"message": "Nenhum turno trabalhado cadastrado"}, HTTPStatus.BAD_REQUEST
    return jsonify(turno_trabalho)


@bp_turno_trabalho.route("/workShift/list", methods=["POST"])
def create_turno_trabalho():

    session = current_app.db.session
    data = request.get_json()

    calendario: CalendarioModel = CalendarioModel.query.get(data["id_calendario"])
    turno: TurnoModel = TurnoModel.query.get(data["id_turno"])

    day = data.pop("dia_da_semana")
    start = data.pop("start_at")
    stop = data.pop("stop_at")

    dayNumber, setDay = set_days(day)

    if not setDay:
        return {
            "message": "dia incorreto.\nopções corretas:1,2,3,4,5,6,7."
        }, HTTPStatus.BAD_REQUEST

    inicioH, inicioM = hora_minuto(start)

    fimH, fimM = hora_minuto(stop)

    date = comparar(inicioH, inicioM, fimH, fimM)

    if not date:
        return {
            "message": "Hora incorreta.\nNão é possivel terminar antes de começar algo."
        }, HTTPStatus.BAD_REQUEST

    new_turno_trabalho: TurnoTrabalhoModel = TurnoTrabalhoModel(
        id_caledario=calendario.id,
        id_turno=turno.id,
        dia_da_semana=dayNumber,
        start_at=start,
        stop_at=stop,
    )

    session.add(new_turno_trabalho)
    session.commit()

    return jsonify(new_turno_trabalho)


@bp_turno_trabalho.route("/workShift/update/<int:id>", methods=["PUT"])
def update_turno_trabalho(id):

    session = current_app.db.session

    data = request.get_json()

    turno_trabalho: TurnoTrabalhoModel = TurnoTrabalhoModel.query.get(id)

    if not turno_trabalho:
        return {"message": "turno trabalhado não encontrado."}, HTTPStatus.NOT_FOUND

    for key, value in data.items():
        setattr(turno_trabalho, key, value)

    session.add(turno_trabalho)
    session.commit()

    return jsonify(turno_trabalho), HTTPStatus.OK


@bp_turno_trabalho.route("/workShift/delete/<int:id>", methods=["DELETE"])
def delete_turno_trabalho(id):

    session = current_app.db.session

    turno_trabalho = TurnoTrabalhoModel()
    query = turno_trabalho.query.get(id)
    if not turno_trabalho:
        return {"message": "Turno trabalhado não encontrado."}, HTTPStatus.NOT_FOUND
    session.delete(query)
    session.commit()

    return "No content", HTTPStatus.NO_CONTENT
