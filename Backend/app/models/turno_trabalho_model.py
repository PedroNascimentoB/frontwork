from sqlalchemy.orm import backref, relationship
from dataclasses import dataclass

from sqlalchemy.sql.schema import ForeignKey
from app.configs.database import db
from sqlalchemy import Column, Integer, String


@dataclass
class TurnoTrabalhoModel(db.Model):
    id: Integer
    start_at: String
    stop_at: String
    list_calendar: "CalendarioModel"

    __tablename__ = "turno_trabalho"

    id = Column(Integer, primary_key=True)

    id_caledario = Column(Integer, ForeignKey("calendario.id"))
    id_turno = Column(Integer, ForeignKey("turnos.id"))

    dia_da_semana = Column(Integer)

    start_at = Column(String)
    stop_at = Column(String)

    list_calendar = relationship(
        "CalendarioModel", backref=backref("turno_trabalho_list")
    )
    list_turnos = relationship("TurnoModel", backref=backref("turno_trabalho_list"))
