from dataclasses import dataclass
from app.configs.database import db
from sqlalchemy import Column, Integer, String


@dataclass
class TurnoModel(db.Model):
    id: Integer
    nome: String
    __tablename__ = "turnos"

    id = Column(Integer, primary_key=True)
    nome = Column(String)
