from dataclasses import dataclass

from sqlalchemy.orm import backref, relationship
from app.configs.database import db
from sqlalchemy import Column, Integer, String


@dataclass
class CalendarioModel(db.Model):
    id: Integer
    nome: String
    turnos_list: "TurnoModel"

    __tablename__ = "calendario"

    id = Column(Integer, primary_key=True)

    nome = Column(String)

    turnos_list = relationship(
        "TurnoModel", backref=backref("calendario_list"), secondary="turno_trabalho"
    )
